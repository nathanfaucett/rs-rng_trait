use core::usize;


pub const INV_MAX_F32: f32 = 1_f32 / usize::MAX as f32;
pub const INV_MAX_F64: f64 = 1_f64 / usize::MAX as f64;


pub trait Rng {

    fn next(&mut self) -> usize;

    #[inline(always)]
    fn next_usize(&mut self) -> usize {
        self.next()
    }

    #[inline(always)]
    fn next_u32(&mut self) -> u32 {
        self.next() as u32
    }
    #[inline(always)]
    fn next_u64(&mut self) -> u64 {
        self.next() as u64
    }

    #[inline(always)]
    fn next_f32(&mut self) -> f32 {
        self.next() as f32 * INV_MAX_F32
    }
    #[inline(always)]
    fn next_f64(&mut self) -> f64 {
        self.next() as f64 * INV_MAX_F64
    }
}


#[cfg(test)]
mod test {
    use super::*;


    struct RngTest {
        seed: u64,
    }

    impl Rng for RngTest {

        fn next(&mut self) -> usize {
            self.seed += 1000000000000000000_u64;
            self.seed as usize
        }
    }

    #[test]
    fn test_next() {
        let mut r = RngTest { seed: 0 };

        assert_eq!(r.next(), 1000000000000000000_u64 as usize);
        assert_eq!(r.next_usize(), 2000000000000000000_u64 as usize);
        assert_eq!(r.next_u32(), 3000000000000000000_u64 as u32);
        assert_eq!(r.next_u64(), 4000000000000000000_u64);
        assert_eq!(r.next_f32(), 0.27105054);
        assert_eq!(r.next_f64(), 0.32526065174565133);
    }
}
